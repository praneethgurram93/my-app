import React, { Component } from 'react';
import Contact from './services/Contact';
import Contacts from './Contacts';

let contacts = [];
class AddContact extends Component {

    contact = new Contact();

    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            contact: '',
            contacts:[]
        }
    
        this.saveFrom = this.saveFrom.bind(this);
    }

    async saveFrom(e) {
        e.preventDefault();
        const {id, name, contact} = this.state;
        console.log(this.state);
        try {
            const request = {id, name, contact}
            const response = await this.contact.registerContact(request);
            this.setState({
                id: '',
                name: '',
                contact: '',  
            }, () => this.setForm)
            const contactResponse = await this.contact.queryContacts();
            this.setState({contacts: contactResponse});
            console.log('Logging contacts', this.state.contacts)
        } catch (e) {
            console.log(e)
        }
    }


    render() {
        const {contacts, id, name, contact} = this.state;
        return (
            <React.Fragment>
                <form>
                    <div className="d-flex justify-content-center mb-4">
                        <div className="col-md-3" style={{border:'2px solid #000000'}}>
                            <div className="form-grooup mt-1">
                                <label htmlFor="id" >ID</label>
                                <input type="text" value={id} onChange={(e) => this.setState({id: e.target.value})} className="form-control mb-2" id="phoneNumberId" placeholder="Id for contact details" />
                            </div>
                            <div className="form-grooup">
                                <label htmlFor="name" >Name</label>
                                <input type="text" value={name} onChange={(e) => this.setState({name: e.target.value})} className="form-control mb-2" id="name" placeholder="Enter name of contact here!." />
                            </div>
                            <div className="form-grooup">
                                <label htmlFor="contact" >Contact</label>
                                <input type="text" value={contact} onChange={(e) => this.setState({contact: e.target.value})} className="form-control mb-2" id="contact" placeholder="Enter the contact number here" />
                            </div>
                            <button type="submit" onClick={(e) => this.saveFrom(e)} className="btn btn-primary mt-2 mb-2">Submit</button>
                        </div>
                    </div>
                </form>
                {contacts.length > 0 && <Contacts />}
            </React.Fragment>
        )
    }
}

export default AddContact;

